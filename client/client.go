// Package main implements a gRPC client that provides access to the
// FPGA memory space on a TS-4200 or TS-4800 board.
//
package main

import (
	pb "bitbucket.org/mfkenney/tsmem"
	"fmt"
	"github.com/urfave/cli"
	"golang.org/x/net/context"
	"google.golang.org/grpc"
	"os"
	"strconv"
	"strings"
	"time"
)

var Version = "dev"
var BuildDate = "unknown"

func bits_to_size(bits int) pb.Size {
	switch bits {
	case 32:
		return pb.Size_LONG
	case 16:
		return pb.Size_WORD
	}
	return pb.Size_BYTE
}

func peek(client pb.TsmemClient, addr uint32, bits int) (uint32, error) {
	ctx, cancel := context.WithTimeout(context.Background(), time.Second*3)
	defer cancel()
	msg := &pb.AddrMsg{Address: addr, Size: bits_to_size(bits)}
	value, err := client.Peek(ctx, msg)
	return value.GetValue(), err
}

func poke(client pb.TsmemClient, addr uint32, bits int,
	value uint32) error {
	ctx, cancel := context.WithTimeout(context.Background(), time.Second*3)
	defer cancel()
	msg := &pb.ValueMsg{
		Address: addr,
		Size:    bits_to_size(bits),
		Value:   value}
	_, err := client.Poke(ctx, msg)
	return err
}

func modify(client pb.TsmemClient, addr uint32, bits int,
	value, mask uint32) (uint32, error) {
	ctx, cancel := context.WithTimeout(context.Background(), time.Second*3)
	defer cancel()
	msg := &pb.UpdateMsg{
		Address: addr,
		Size:    bits_to_size(bits),
		Value:   value,
		Mask:    mask}
	resp, err := client.Modify(ctx, msg)
	return resp.GetValue(), err
}

func dioset(client pb.TsmemClient, name string, state pb.DioState) error {
	ctx, cancel := context.WithTimeout(context.Background(), time.Second*3)
	defer cancel()
	msg := &pb.DioMsg{
		Name:  name,
		State: state}
	_, err := client.DioSet(ctx, msg)
	return err
}

func dioget(client pb.TsmemClient, name string) (pb.DioState, error) {
	ctx, cancel := context.WithTimeout(context.Background(), time.Second*3)
	defer cancel()
	msg := &pb.DioMsg{Name: name}
	resp, err := client.DioGet(ctx, msg)
	if err != nil {
		return pb.DioState_LOW, err
	}
	return resp.GetState(), nil
}

func main() {
	var opts []grpc.DialOption
	opts = append(opts, grpc.WithInsecure())

	app := cli.NewApp()
	app.Name = "tsmem"
	app.Usage = "access the FPGA memory on a TS CPU board"
	app.Version = Version

	app.Flags = []cli.Flag{
		cli.StringFlag{
			Name:  "server, s",
			Usage: "Server address, host:port",
			Value: "127.0.0.1:10000",
		},
	}

	var conn *grpc.ClientConn
	var client pb.TsmemClient

	app.Before = func(c *cli.Context) error {
		var err error
		conn, err = grpc.Dial(c.String("server"), opts...)
		if err == nil {
			client = pb.NewTsmemClient(conn)
		}
		return err
	}

	app.After = func(c *cli.Context) error {
		if conn != nil {
			conn.Close()
		}
		return nil
	}

	app.Commands = []cli.Command{
		{
			Name:            "peek",
			Usage:           "read a memory location",
			SkipFlagParsing: true,
			ArgsUsage:       "addr bits",
			Action: func(c *cli.Context) error {
				if c.NArg() < 2 {
					return fmt.Errorf("Missing arguments")
				}
				addr, err := strconv.ParseInt(c.Args().Get(0), 0, 0)
				if err != nil {
					return cli.NewExitError(err, 1)
				}
				bits, err := strconv.ParseInt(c.Args().Get(1), 0, 0)
				if err != nil {
					return cli.NewExitError(err, 1)
				}
				value, err := peek(client, uint32(addr), int(bits))
				if err != nil {
					return cli.NewExitError(err, 2)
				}
				fmt.Fprintf(c.App.Writer, "0x%08x\n", value)
				return nil
			},
		},
		{
			Name:            "poke",
			Usage:           "write to a memory location",
			SkipFlagParsing: true,
			ArgsUsage:       "addr bits value",
			Action: func(c *cli.Context) error {
				if c.NArg() < 3 {
					return fmt.Errorf("Missing arguments")
				}
				addr, err := strconv.ParseInt(c.Args().Get(0), 0, 0)
				if err != nil {
					return cli.NewExitError(err, 1)
				}
				bits, err := strconv.ParseInt(c.Args().Get(1), 0, 0)
				if err != nil {
					return cli.NewExitError(err, 1)
				}
				value, err := strconv.ParseInt(c.Args().Get(2), 0, 0)
				if err != nil {
					return cli.NewExitError(err, 1)
				}
				err = poke(client, uint32(addr), int(bits), uint32(value))
				if err != nil {
					return cli.NewExitError(err, 2)
				}
				return nil
			},
		},
		{
			Name:            "bitset16",
			Usage:           "set one or more bits of a memory location",
			SkipFlagParsing: true,
			ArgsUsage:       "addr bit [bit ...]",
			Action: func(c *cli.Context) error {
				if c.NArg() < 2 {
					return fmt.Errorf("Missing arguments")
				}
				addr, err := strconv.ParseInt(c.Args().Get(0), 0, 0)
				mask := uint32(0)
				for i := 1; i < c.NArg(); i++ {
					bit, err := strconv.ParseInt(c.Args().Get(i), 0, 0)
					if err != nil {
						return cli.NewExitError(err, 1)
					}
					mask = mask | (uint32(1) << uint32(bit))
				}
				_, err = modify(client, uint32(addr), 16,
					uint32(mask)&0xffff, uint32(mask)&0xffff)
				if err != nil {
					return cli.NewExitError(err, 2)
				}
				return nil
			},
		},
		{
			Name:            "bitclear16",
			Usage:           "clear one or more bits of a memory location",
			SkipFlagParsing: true,
			ArgsUsage:       "addr bit [bit ...]",
			Action: func(c *cli.Context) error {
				if c.NArg() < 2 {
					return fmt.Errorf("Missing arguments")
				}
				addr, err := strconv.ParseInt(c.Args().Get(0), 0, 0)
				mask := uint32(0)
				for i := 1; i < c.NArg(); i++ {
					bit, err := strconv.ParseInt(c.Args().Get(i), 0, 0)
					if err != nil {
						return cli.NewExitError(err, 1)
					}
					mask = mask | (uint32(1) << uint32(bit))
				}
				_, err = modify(client, uint32(addr), 16,
					uint32(0), uint32(mask)&0xffff)
				if err != nil {
					return cli.NewExitError(err, 2)
				}
				return nil
			},
		},
		{
			Name:            "dioset",
			Usage:           "change the state of a DIO line",
			SkipFlagParsing: true,
			ArgsUsage:       "name state",
			Action: func(c *cli.Context) error {
				if c.NArg() < 2 {
					return fmt.Errorf("Missing arguments")
				}
				state, ok := pb.DioState_value[strings.ToUpper(c.Args().Get(1))]
				if !ok {
					return fmt.Errorf("Invalid DIO state: %q", c.Args().Get(1))
				}
				err := dioset(client, strings.ToUpper(c.Args().Get(0)),
					pb.DioState(state))
				if err != nil {
					return cli.NewExitError(err, 2)
				}
				return nil
			},
		},
		{
			Name:            "dioget",
			Usage:           "read the state of a DIO line",
			SkipFlagParsing: true,
			ArgsUsage:       "name",
			Action: func(c *cli.Context) error {
				if c.NArg() < 1 {
					return fmt.Errorf("Missing arguments")
				}
				state, err := dioget(client, strings.ToUpper(c.Args().Get(0)))
				if err == nil {
					fmt.Fprintf(c.App.Writer, "%s=%s\n", c.Args().Get(0),
						pb.DioState_name[int32(state)])
					return nil
				}

				return cli.NewExitError(err, 2)
			},
		},
	}

	app.Run(os.Args)
}
