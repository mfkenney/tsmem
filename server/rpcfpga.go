// Package main implements a gRPC server that provides access to the
// FPGA memory space on a TS-4200 or TS-4800 board.
//
package main

import (
	pb "bitbucket.org/mfkenney/tsmem"
	"flag"
	"fmt"
	"golang.org/x/net/context"
	"google.golang.org/grpc"
	"google.golang.org/grpc/grpclog"
	"io/ioutil"
	"log"
	"math/rand"
	"net"
	"os"
	"os/signal"
	"runtime"
	"sync"
	"syscall"
)

var Version = "dev"
var BuildDate = "unknown"

type dio struct {
	board_id int16
	dir_reg  uint32
	out_reg  uint32
	in_reg   uint32
	mask     int16
}

var (
	port     = flag.Int("port", 10000, "The server port")
	testfile = flag.String("testfile", "",
		"File containing fake data for the \"dummy board\"")
	showvers = flag.Bool("version", false,
		"Show program version information and exit")
)

var boards = map[string]struct {
	base uint32
	size uint32
}{
	"ts4200": {base: 0x30000000, size: 0x800},
	"ts4800": {base: 0xb0010000, size: 0x8000},
	"dummy":  {base: 0, size: 0x1000},
}

// Use the same DIO names as tsctl
var diomap = map[string]dio{
	"8160_LCD_D0": {0x4200, 0x40a, 0x406, 0x40e, 0x01},
	"8160_LCD_D1": {0x4200, 0x40a, 0x406, 0x40e, 0x02},
	"8160_LCD_D2": {0x4200, 0x40a, 0x406, 0x40e, 0x04},
	"8160_LCD_D3": {0x4200, 0x40a, 0x406, 0x40e, 0x08},
	"8160_LCD_D4": {0x4200, 0x40a, 0x406, 0x40e, 0x10},
	"8160_LCD_D5": {0x4200, 0x40a, 0x406, 0x40e, 0x20},
	"8160_LCD_D6": {0x4200, 0x40a, 0x406, 0x40e, 0x40},
	"8160_LCD_D7": {0x4200, 0x40a, 0x406, 0x40e, 0x80},
	"8100_LCD_D0": {0x4800, 0x100a, 0x1006, 0x100e, 0x01},
	"8100_LCD_D1": {0x4800, 0x100a, 0x1006, 0x100e, 0x02},
	"130":         {0x4800, 0x1008, 0x1004, 0x100c, 0x01},
	"131":         {0x4800, 0x1008, 0x1004, 0x100c, 0x02},
	"132":         {0x4800, 0x1008, 0x1004, 0x100c, 0x04},
	"133":         {0x4800, 0x1008, 0x1004, 0x100c, 0x08},
}

type tsmemServer struct {
	mbuf   []byte
	offset uint32
	mu     *sync.Mutex
}

func unmapMem(s *tsmemServer) error {
	if s.mbuf == nil {
		return nil
	}
	mbuf := s.mbuf
	s.mbuf = nil
	runtime.SetFinalizer(s, nil)
	return syscall.Munmap(mbuf)
}

func newServer(base, size uint32) *tsmemServer {
	f, err := os.OpenFile("/dev/mem", os.O_RDWR|os.O_SYNC, 0660)
	if err != nil {
		return nil
	}
	defer f.Close()

	// The mmap'ed buffer must start on a page boundary
	page := int64(base &^ 0xfff)
	s := new(tsmemServer)
	s.offset = base - uint32(page)
	s.mbuf, err = syscall.Mmap(int(f.Fd()),
		page,
		int(s.offset+size),
		syscall.PROT_READ|syscall.PROT_WRITE,
		syscall.MAP_SHARED)
	if err != nil {
		grpclog.Printf("mmap failed: %v", err)
		return nil
	}
	s.mu = &sync.Mutex{}
	runtime.SetFinalizer(s, unmapMem)
	return s
}

func dummyServer(size uint32, datafile string) *tsmemServer {
	var err error
	s := new(tsmemServer)
	s.offset = 0

	if datafile == "" {
		s.mbuf = make([]byte, size, size)
		r := rand.New(rand.NewSource(42))
		r.Read(s.mbuf)
	} else {
		s.mbuf, err = ioutil.ReadFile(datafile)
		if err != nil {
			grpclog.Printf("Cannot read data file: %v", err)
			return nil
		}
	}
	s.mu = &sync.Mutex{}

	return s
}

func (s *tsmemServer) check_range(addr uint32) error {
	if int(addr+s.offset) >= cap(s.mbuf) {
		return fmt.Errorf("Invalid address: 0x%08x", addr)
	}
	return nil
}

func (s *tsmemServer) DioGet(ctx context.Context,
	msg *pb.DioMsg) (*pb.DioMsg, error) {
	line, ok := diomap[msg.GetName()]
	if !ok {
		return nil, fmt.Errorf("Invalid name: %q", msg.GetName())
	}

	if s.read_word(0) != line.board_id {
		return nil, fmt.Errorf("CPU board mismatch")
	}

	resp := &pb.DioMsg{Name: msg.GetName()}
	if (s.read_word(line.dir_reg) & line.mask) == line.mask {
		if (s.read_word(line.out_reg) & line.mask) == line.mask {
			resp.State = pb.DioState_HIGH
		} else {
			resp.State = pb.DioState_LOW
		}
	} else {
		if (s.read_word(line.in_reg) & line.mask) == line.mask {
			resp.State = pb.DioState_INPUT_HIGH
		} else {
			resp.State = pb.DioState_INPUT_LOW
		}
	}

	return resp, nil
}

func (s *tsmemServer) DioSet(ctx context.Context,
	msg *pb.DioMsg) (*pb.DioMsg, error) {
	line, ok := diomap[msg.GetName()]
	if !ok {
		return nil, fmt.Errorf("Invalid name: %q", msg.GetName())
	}

	if s.read_word(0) != line.board_id {
		return nil, fmt.Errorf("CPU board mismatch")
	}

	resp := &pb.DioMsg{Name: msg.GetName(), State: msg.GetState()}
	switch msg.GetState() {
	case pb.DioState_HIGH:
		s.mod_word(line.out_reg, line.mask, line.mask)
		s.mod_word(line.dir_reg, line.mask, line.mask)
	case pb.DioState_LOW:
		s.mod_word(line.out_reg, 0, line.mask)
		s.mod_word(line.dir_reg, line.mask, line.mask)
	case pb.DioState_INPUT:
		s.mod_word(line.dir_reg, 0, line.mask)
	}

	return resp, nil
}

func (s *tsmemServer) Peek(ctx context.Context,
	msg *pb.AddrMsg) (*pb.ValueMsg, error) {

	addr := msg.GetAddress()
	err := s.check_range(addr)
	if err != nil {
		grpclog.Println(err)
		return nil, err
	}

	vm := &pb.ValueMsg{Address: addr}
	switch size := msg.GetSize(); size {
	case pb.Size_BYTE:
		vm.Value = uint32(s.read_byte(addr)) & 0xff
		vm.Size = size
	case pb.Size_WORD:
		vm.Value = uint32(s.read_word(addr)) & 0xffff
		vm.Size = size
	case pb.Size_LONG:
		vm.Value = uint32(s.read_long(addr))
		vm.Size = size
	}
	return vm, nil
}

func (s *tsmemServer) Poke(ctx context.Context,
	msg *pb.ValueMsg) (*pb.ValueMsg, error) {

	addr := msg.GetAddress()
	val := msg.GetValue()
	err := s.check_range(addr)
	if err != nil {
		grpclog.Println(err)
		return nil, err
	}

	vm := &pb.ValueMsg{Address: addr}
	switch size := msg.GetSize(); size {
	case pb.Size_BYTE:
		vm.Value = uint32(s.write_byte(addr, int8(val&0xff))) & 0xff
		vm.Size = size
	case pb.Size_WORD:
		vm.Value = uint32(s.write_word(addr, int16(val&0xffff))) & 0xffff
		vm.Size = size
	case pb.Size_LONG:
		vm.Value = uint32(s.write_long(addr, int32(val)))
		vm.Size = size
	}
	return vm, nil
}

func (s *tsmemServer) Modify(ctx context.Context,
	msg *pb.UpdateMsg) (*pb.ValueMsg, error) {

	addr := msg.GetAddress()
	val := msg.GetValue()
	mask := msg.GetMask()
	err := s.check_range(addr)
	if err != nil {
		grpclog.Println(err)
		return nil, err
	}

	vm := &pb.ValueMsg{Address: addr}
	switch size := msg.GetSize(); size {
	case pb.Size_BYTE:
		vm.Value = uint32(s.mod_byte(addr, int8(val&0xff), int8(mask&0xff))) & 0xff
		vm.Size = size
	case pb.Size_WORD:
		vm.Value = uint32(s.mod_word(addr, int16(val&0xffff), int16(mask&0xffff))) & 0xffff
		vm.Size = size
	case pb.Size_LONG:
		vm.Value = uint32(s.mod_long(addr, int32(val), int32(mask)))
		vm.Size = size
	}
	return vm, nil
}

func main() {
	flag.Usage = func() {
		fmt.Fprintf(os.Stderr, "Usage: %s [options] board\n",
			os.Args[0])
		fmt.Fprintf(os.Stderr, "RPC server to allow access to FPGA memory space.\n\n")
		flag.PrintDefaults()
	}

	flag.Parse()
	if *showvers {
		fmt.Fprintf(os.Stderr, "%s %s\n", os.Args[0], Version)
		fmt.Fprintf(os.Stderr, "  Build date: %s\n", BuildDate)
		fmt.Fprintf(os.Stderr, "  Built with: %s\n", runtime.Version())
		os.Exit(0)
	}

	args := flag.Args()

	if len(args) < 1 {
		flag.Usage()
		os.Exit(1)
	}

	board, ok := boards[args[0]]
	if !ok {
		grpclog.Fatalf("Unknown board name: %q", args[0])
	}

	lis, err := net.Listen("tcp", fmt.Sprintf(":%d", *port))
	if err != nil {
		grpclog.Fatalf("failed to listen: %v", err)
	}

	// Initialize signal handler
	sigs := make(chan os.Signal, 1)
	signal.Notify(sigs, syscall.SIGINT,
		syscall.SIGTERM, syscall.SIGHUP, syscall.SIGPIPE)
	defer signal.Stop(sigs)

	grpcServer := grpc.NewServer()
	if board.base == 0 {
		pb.RegisterTsmemServer(grpcServer, dummyServer(board.size, *testfile))
	} else {
		pb.RegisterTsmemServer(grpcServer, newServer(board.base, board.size))
	}

	// Goroutine to wait for signals or for the signal channel
	// to close.
	go func() {
		s, more := <-sigs
		if more {
			log.Printf("Got signal %v", s)
			grpcServer.GracefulStop()
		}
	}()

	grpcServer.Serve(lis)
}
