//
package main

import (
	"unsafe"
)

func (s *tsmemServer) read_byte(addr uint32) int8 {
	p := (*int8)(unsafe.Pointer(&s.mbuf[addr+s.offset]))
	return *p
}

func (s *tsmemServer) read_word(addr uint32) int16 {
	p := (*int16)(unsafe.Pointer(&s.mbuf[addr+s.offset]))
	return *p
}

func (s *tsmemServer) read_long(addr uint32) int32 {
	p := (*int32)(unsafe.Pointer(&s.mbuf[addr+s.offset]))
	return *p
}

func (s *tsmemServer) write_byte(addr uint32, value int8) int8 {
	p := (*int8)(unsafe.Pointer(&s.mbuf[addr+s.offset]))
	old := *p
	*p = value
	return old
}

func (s *tsmemServer) write_word(addr uint32, value int16) int16 {
	p := (*int16)(unsafe.Pointer(&s.mbuf[addr+s.offset]))
	old := *p
	*p = value
	return old
}

func (s *tsmemServer) write_long(addr uint32, value int32) int32 {
	p := (*int32)(unsafe.Pointer(&s.mbuf[addr+s.offset]))
	old := *p
	*p = value
	return old
}

func (s *tsmemServer) mod_byte(addr uint32, value, mask int8) int8 {
	s.mu.Lock()
	p := (*int8)(unsafe.Pointer(&s.mbuf[addr+s.offset]))
	old := *p
	*p = (old &^ mask) | (value & mask)
	s.mu.Unlock()
	return old
}

func (s *tsmemServer) mod_word(addr uint32, value, mask int16) int16 {
	s.mu.Lock()
	p := (*int16)(unsafe.Pointer(&s.mbuf[addr+s.offset]))
	old := *p
	*p = (old &^ mask) | (value & mask)
	s.mu.Unlock()
	return old
}

func (s *tsmemServer) mod_long(addr uint32, value, mask int32) int32 {
	s.mu.Lock()
	p := (*int32)(unsafe.Pointer(&s.mbuf[addr+s.offset]))
	old := *p
	*p = (old &^ mask) | (value & mask)
	s.mu.Unlock()
	return old
}
