# Access FPGA memory on a Technologic Systems SOC

This [gRPC](http://www.grpc.io) service provides access to the FPGA memory
space on a TS-4200 or TS-4800 Technologic Systems SOC (System On A
Chip). Similar access is provided by the vendor
provided [tsctl program](https://wiki.embeddedarm.com/wiki/Tsctl) but
`tsctl` has proven to be "less than reliable".
