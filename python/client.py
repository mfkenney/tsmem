#!/usr/bin/env python

from __future__ import print_function
import click
import grpc
import tsmem_pb2
import tsmem_pb2_grpc


@click.command()
@click.argument('server')
def cli(server):
    channel = grpc.insecure_channel(server)
    stub = tsmem_pb2_grpc.PeekPokeStub(channel)
    addr = tsmem_pb2.AddrMsg(size=tsmem_pb2.WORD)
    for i in range(8):
        addr.address = i
        result = stub.Peek(addr)
        print('Value[{}]=0x{:04x}'.format(i, result.value))


if __name__ == '__main__':
    cli()
